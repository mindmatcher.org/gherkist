'use strict'

module.exports = wrapper


//TODO : see if we keep that
// Wrapper to normalise options.
function wrapper(ast, options) {
  var settings = options || {}
  var file

  if (settings.messages) {
    file = settings
    settings = {}
  } else {
    file = settings.file
  }

  //console.log('... in the wrapper')
  const r = transform(ast, {
    schema: 'to define',
    file: file,
    verbose: settings.verbose,
    location: false
  })
  return r
}

// Transform a node.
function transform(ast, config) {

  console.log('==')
  console.log('original AST')
  console.log(ast)
  console.log('==')


  let rootNode = {
    type : 'root',
    children: []
  }

  objectList(ast, rootNode)


  return rootNode
 
}


function objectList(sourceObj, targetObj, baseType = null){

  if(!targetObj.children) targetObj.children = []

  Object.entries(sourceObj).forEach(([key, value]) => {
    if( key === 'GherkinDocument'){
      targetObj.children.push(...processChildren(value))
      return
    }
    if( key === 'comments'){
      targetObj.children.push(...processChildren(value, {type : 'comment'}))
      return 
    }
    if( key === 'feature'){
      targetObj.children.push(...processChildren([value]))
      return 
    }
    //warning this one is specific
    if( key === 'keyword') {
      targetObj['type'] = baseType ? baseType : value.toLowerCase()
      targetObj['keyword'] = value
      return
    }
    if( key === 'children') {
      targetObj.children.push(...processChildren(value))
      return 
    }
    if( key === 'steps'){
      targetObj.children.push(...processChildren(value, { type : 'step'}))
      return 
    }
    
    if( key === 'tags'){
      targetObj.children.push(...processChildren(value, { type : 'tag'}))
      return
    }
    

    targetObj[key] = value
  });

  if(!targetObj.children) targetObj.children = [];

}

function processChildren(values, baseObject = {}){
  let r = [] 
  values.forEach( v => {
    let t = {...baseObject}

    v = v.background ? v.background : v ;
    v = v.scenario ? v.scenario : v ;

    objectList(v,t)
    r.push(t)
  })
  return r;
}
